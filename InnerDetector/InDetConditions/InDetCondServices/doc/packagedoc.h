/*
  Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration
*/

/**
@page InDetCondServices_page InDetCondServices package

@section InDetCondServices_InDetCondServicesIntroduction Introduction

This package provides interfaces to conditions services.

*/
